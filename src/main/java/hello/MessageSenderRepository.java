package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MessageSenderRepository extends CrudRepository<MessageSender, Integer> {
    Optional<MessageSender> findByMessageid(int message_id);

    Iterable<MessageSender> getAllByIsDeliveredEquals(String isDelivered);
}
