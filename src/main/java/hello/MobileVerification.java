package hello;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "MOBILE_VERIFICATION")
public class MobileVerification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "mob_id")
    private Integer mobileId;
    private String mobile;
    private String otp;
    private String verification_status;
    private Timestamp expiration_time;

    public MobileVerification() {
    }

    public Integer getMobileId() {
        return mobileId;
    }

    public void setMobileId(Integer mobileId) {
        this.mobileId = mobileId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(String verification_status) {
        this.verification_status = verification_status;
    }

    public Timestamp getExpiration_time() {
        return expiration_time;
    }

    public void setExpiration_time(Timestamp expiration_time) {
        this.expiration_time = expiration_time;
    }
}
