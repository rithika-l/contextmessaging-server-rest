package hello;

import org.springframework.data.repository.CrudRepository;

public interface StepsGoalRepository extends CrudRepository<StepsGoal, Integer> {

}
