package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MessagingTokenRepository extends CrudRepository<MessagingToken, Integer> {
    public Optional<MessagingToken> findByUserId(int userId);
}
