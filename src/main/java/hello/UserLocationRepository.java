package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserLocationRepository extends CrudRepository<UserLocation, Integer> {
    List<UserLocation> findAllByUserId(int userId);
}
