package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "TRIGGER_GOAL")
public class GoalTrigger {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer trigger_goal_id;

    private Integer message_id;
    private String rule;
    private Integer goal_id;

    public Integer getTrigger_goal_id() {
        return trigger_goal_id;
    }

    public void setTrigger_goal_id(Integer trigger_goal_id) {
        this.trigger_goal_id = trigger_goal_id;
    }

    public Integer getMessage_id() {
        return message_id;
    }

    public void setMessage_id(Integer message_id) {
        this.message_id = message_id;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public Integer getGoal_id() {
        return goal_id;
    }

    public void setGoal_id(Integer goal_id) {
        this.goal_id = goal_id;
    }
}
