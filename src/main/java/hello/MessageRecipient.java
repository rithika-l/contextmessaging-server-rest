package hello;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "MESSAGE_RECIPIENT")
public class MessageRecipient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer message_recipient_id;

    private String message_recipient_uid;

    private Integer recipient_id;
    private String recipient_group_id;
    @Column(name = "message_id", columnDefinition = "BINARY(16)")
    private UUID messageid;
    private String notification_sent;
    private String is_read;

    public Integer getMessage_recipient_id() {
        return message_recipient_id;
    }

    public void setMessage_recipient_id(Integer message_recipient_id) {
        this.message_recipient_id = message_recipient_id;
    }

    public Integer getRecipient_id() {
        return recipient_id;
    }

    public void setRecipient_id(Integer recipient_id) {
        this.recipient_id = recipient_id;
    }

    public String getRecipient_group_id() {
        return recipient_group_id;
    }

    public void setRecipient_group_id(String recipient_group_id) {
        this.recipient_group_id = recipient_group_id;
    }


    public String getNotification_sent() {
        return notification_sent;
    }

    public void setNotification_sent(String notification_sent) {
        this.notification_sent = notification_sent;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public UUID getMessageid() {
        return messageid;
    }

    public void setMessageid(UUID message_id) {
        this.messageid = message_id;
    }

    public String getMessage_recipient_uid() {
        return message_recipient_uid;
    }

    public void setMessage_recipient_uid(String message_recipient_uid) {
        this.message_recipient_uid = message_recipient_uid;
    }
}
