package hello.Utils;

import hello.TimeTrigger;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

public class Util {

    public static String convertToTimestamp(TimeTrigger t){
        if(t.getTime()!=null && t.getDay_of_week()==null)
            return convertTimeToStringFormat(t.getTime());
        else
            return dayOfWeekToTimestamp(t.getDay_of_week(), t.getTime());
    }

    public static String dayOfWeekToTimestamp(int num, String time){
        LocalDate date;
        DateTimeFormatter dtfDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        switch(num){
            case 0: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))                   // Capture the current date as seen by the people in a certain region (time zone).
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY));
                String mondayDate = dtfDate.format(date);
                if(time==null)
                    mondayDate = mondayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("monday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        mondayDate = dtfDate.format(date);
                    }
                    mondayDate = mondayDate + " " + time;
                }
                return mondayDate;
            }
            case 1: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.TUESDAY));
                String tuesdayDate = dtfDate.format(date);
                if(time==null)
                    tuesdayDate = tuesdayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("tuesday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        tuesdayDate = dtfDate.format(date);
                    }
                    tuesdayDate = tuesdayDate + " " + time;
                }
                return tuesdayDate;
            }
            case 2: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.WEDNESDAY));
                String wednesdayDate = dtfDate.format(date);
                if(time==null)
                    wednesdayDate = wednesdayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("wednesday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        wednesdayDate = dtfDate.format(date);
                    }
                    wednesdayDate = wednesdayDate + " " + time;
                }
                return wednesdayDate;
            }
            case 3: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.THURSDAY));
                String thursdayDate = dtfDate.format(date);
                if(time==null)
                    thursdayDate = thursdayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("thursday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        thursdayDate = dtfDate.format(date);
                    }
                    thursdayDate = thursdayDate + " " + time;
                }
                return thursdayDate;
            }
            case 4: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY));
                String fridayDate = dtfDate.format(date);
                if(time==null)
                    fridayDate = fridayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("friday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        fridayDate = dtfDate.format(date);
                    }
                    fridayDate = fridayDate + " " + time;
                }
                return fridayDate;
            }
            case 5: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));
                String saturdayDate = dtfDate.format(date);
                if(time==null)
                    saturdayDate = saturdayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("saturday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        saturdayDate = dtfDate.format(date);
                    }
                    saturdayDate = saturdayDate + " " + time;
                }
                return saturdayDate;
            }
            case 6: {
                date = LocalDate.now(ZoneId.of("America/Montreal"))
                        .with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
                String sundayDate = dtfDate.format(date);
                if(time==null)
                    sundayDate = sundayDate + " 12:00:00";
                else {
                    if(LocalDate.now().getDayOfWeek().name().equalsIgnoreCase("sunday") && isTimeInPast(time)) {
                        date = date.plusDays(7);
                        sundayDate = dtfDate.format(date);
                    }
                    sundayDate = sundayDate + " " + time;
                }
                return sundayDate;
            }
            default:
                return null;
        }
    }

    public static String convertTimeToStringFormat(String time){
        DateTimeFormatter dtfDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();

        if(isTimeInPast(time)) {
            localDate = localDate.plusDays(1);
        }

        String finalDate = dtfDate.format(localDate);

        return finalDate + " " + time;
    }

    public static boolean isTimeInPast(String time){
        LocalTime givenTime = LocalTime.parse(time);
        LocalTime now = LocalTime.now();
        return now.isAfter(givenTime);
    }
}
