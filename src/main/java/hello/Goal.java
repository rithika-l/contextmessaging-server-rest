package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name = "GOALS")
public class Goal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer goal_id;

    private Integer user_id;
    private Timestamp goal_start_date;
    private Timestamp goal_end_time;
    private String goal_type;

    public Integer getGoal_id() {
        return goal_id;
    }

    public void setGoal_id(Integer goal_id) {
        this.goal_id = goal_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Timestamp getGoal_start_date() {
        return goal_start_date;
    }

    public void setGoal_start_date(Timestamp goal_start_date) {
        this.goal_start_date = goal_start_date;
    }

    public Timestamp getGoal_end_time() {
        return goal_end_time;
    }

    public void setGoal_end_time(Timestamp goal_end_time) {
        this.goal_end_time = goal_end_time;
    }

    public String getGoal_type() {
        return goal_type;
    }

    public void setGoal_type(String goal_type) {
        this.goal_type = goal_type;
    }
}
