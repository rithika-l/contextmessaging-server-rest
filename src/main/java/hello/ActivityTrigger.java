package hello;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "TRIGGER_ACTIVITY")
public class ActivityTrigger {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer trigger_activity_id;

    @Column(name = "message_id")
    private Integer messageId;
    private String rule;
    private Timestamp expire_time;
    @Column(name = "is_delivered")
    private String isDelivered;

    public Integer getTrigger_activity_id() {
        return trigger_activity_id;
    }

    public void setTrigger_activity_id(Integer trigger_activity_id) {
        this.trigger_activity_id = trigger_activity_id;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer message_id) {
        this.messageId = message_id;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public Timestamp getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(Timestamp expire_time) {
        this.expire_time = expire_time;
    }

    public String getIsDelivered() {
        return isDelivered;
    }

    public void setIsDelivered(String is_delivered) {
        this.isDelivered = is_delivered;
    }
}
