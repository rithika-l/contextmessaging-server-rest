package hello;

import javax.persistence.*;

@Entity(name = "TRIGGER_LOCATION")
public class LocationTrigger {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer trigger_location_id;

    @Column(name = "message_id")
    private Integer messageId;
    private String rule;
    private Float latitude;
    private Float longitude;
    private Float radius;
    private String place;

    @Column(name = "is_delivered")
    private String isDelivered;

    public Integer getTrigger_location_id() {
        return trigger_location_id;
    }

    public void setTrigger_location_id(Integer trigger_location_id) {
        this.trigger_location_id = trigger_location_id;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer message_id) {
        this.messageId = message_id;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getRadius() {
        return radius;
    }

    public void setRadius(Float radius) {
        this.radius = radius;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getIsDelivered() {
        return isDelivered;
    }

    public void setIsDelivered(String isDelivered) {
        this.isDelivered = isDelivered;
    }
}
