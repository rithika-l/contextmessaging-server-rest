package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface MessageRecipientRepository extends CrudRepository<MessageRecipient,Integer> {
    Optional<MessageRecipient> findByMessageid(UUID messageid);
}
