package hello;

import org.springframework.data.repository.CrudRepository;

public interface GoalTriggerRepository extends CrudRepository<GoalTrigger, Integer> {

}
