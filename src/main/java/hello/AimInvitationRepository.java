package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AimInvitationRepository extends CrudRepository<AimInvitation, Integer> {
    Optional<AimInvitation> findByUserId(int userId);
}
