package hello;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "TRIGGER_TIME")
public class TimeTrigger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer trigger_time_id;

    @Column(name = "message_id")
    private Integer messageId;
    private String rule;
    private String time;
    private Integer day_of_week;

    @Column(name = "scheduled_timestamp")
    private Timestamp scheduledTimestamp;

    @Column(name = "mul_triggers")
    private String mulTriggers;

    private String timezone;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/New_York")
    private Timestamp expire_time;

    private String frequency_type;
    private Integer frequency_interval;
    private String frequency_monday;
    private String frequency_tuesday;
    private String frequency_wednesday;
    private String frequency_thursday;
    private String frequency_friday;
    private String frequency_saturday;
    private String frequency_sunday;

    public Integer getTrigger_time_id() {
        return trigger_time_id;
    }

    public void setTrigger_time_id(Integer trigger_time_id) {
        this.trigger_time_id = trigger_time_id;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer message_id) {
        this.messageId = message_id;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Timestamp getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(Timestamp expire_time) {
        this.expire_time = expire_time;
    }

    public String getFrequency_type() {
        return frequency_type;
    }

    public void setFrequency_type(String frequency_type) {
        this.frequency_type = frequency_type;
    }

    public Integer getFrequency_interval() {
        return frequency_interval;
    }

    public void setFrequency_interval(Integer frequency_interval) {
        this.frequency_interval = frequency_interval;
    }

    public String getFrequency_monday() {
        return frequency_monday;
    }

    public void setFrequency_monday(String frequency_monday) {
        this.frequency_monday = frequency_monday;
    }

    public String getFrequency_tuesday() {
        return frequency_tuesday;
    }

    public void setFrequency_tuesday(String frequency_tuesday) {
        this.frequency_tuesday = frequency_tuesday;
    }

    public String getFrequency_wednesday() {
        return frequency_wednesday;
    }

    public void setFrequency_wednesday(String frequency_wednesday) {
        this.frequency_wednesday = frequency_wednesday;
    }

    public String getFrequency_thursday() {
        return frequency_thursday;
    }

    public void setFrequency_thursday(String frequency_thursday) {
        this.frequency_thursday = frequency_thursday;
    }

    public String getFrequency_friday() {
        return frequency_friday;
    }

    public void setFrequency_friday(String frequency_friday) {
        this.frequency_friday = frequency_friday;
    }

    public String getFrequency_saturday() {
        return frequency_saturday;
    }

    public void setFrequency_saturday(String frequency_saturday) {
        this.frequency_saturday = frequency_saturday;
    }

    public String getFrequency_sunday() {
        return frequency_sunday;
    }

    public void setFrequency_sunday(String frequency_sunday) {
        this.frequency_sunday = frequency_sunday;
    }

    public Integer getDay_of_week() {
        return day_of_week;
    }

    public void setDay_of_week(Integer day_of_week) {
        this.day_of_week = day_of_week;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Timestamp getScheduledTimestamp() {
        return scheduledTimestamp;
    }

    public void setScheduledTimestamp(Timestamp scheduled_timestamp) {
        this.scheduledTimestamp = scheduled_timestamp;
    }

    public String getMulTriggers() {
        return mulTriggers;
    }

    public void setMulTriggers(String mul_triggers) {
        this.mulTriggers = mul_triggers;
    }
}
