package hello;

import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.Optional;

public interface TimeTriggerRepository extends CrudRepository<TimeTrigger, Integer> {
    Iterable<TimeTrigger> findAllByScheduledTimestampBetweenAndMulTriggersEquals(Timestamp t1, Timestamp t2, String mulTriggers);

    Iterable<TimeTrigger> findAllByScheduledTimestampBeforeAndMulTriggersEquals(Timestamp t1, String mulTriggers);

    Optional<TimeTrigger> findByMessageId(int messageId);
}
