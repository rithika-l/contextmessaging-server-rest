package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "WALKING")
public class WalkingGoal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer walking_id;

    private Integer goal_id;
    private Integer distance;
    private Integer actual_distance;
    private Integer duration;

    public Integer getWalking_id() {
        return walking_id;
    }

    public void setWalking_id(Integer walking_id) {
        this.walking_id = walking_id;
    }

    public Integer getGoal_id() {
        return goal_id;
    }

    public void setGoal_id(Integer goal_id) {
        this.goal_id = goal_id;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getActual_distance() {
        return actual_distance;
    }

    public void setActual_distance(Integer actual_distance) {
        this.actual_distance = actual_distance;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
