package hello.models;

public class SMSDetails {
    public String messageContents;

    public SMSDetails(){
    }

    public String getHash() {
        return "+GWDB0sfIc8";
    }

    public String getMessageContents() {
        return messageContents;
    }

    public void setMessageContents(String otp, String hash) {
        this.messageContents = "The verification code is " + otp + " for SCC Messaging. \n\n" + hash;
    }
}
