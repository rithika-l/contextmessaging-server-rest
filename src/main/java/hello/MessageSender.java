package hello;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity(name="MESSAGES")
public class MessageSender {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    private Integer messageid;

    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Generated(GenerationTime.ALWAYS)
    @Column(columnDefinition = "BINARY(16)")
    private UUID message_uid;

    private Integer sender_id;
    private String subject;

    private String body;

    @Lob
    @Column(name = "attachment", columnDefinition = "BLOB")
    private byte[] attachment;

    private Timestamp sent_date;

    @Column(name = "is_delivered")
    private String isDelivered;
    private Integer parent_message_id;

    private String trigger_name;

    public Integer getMessage_id() {
        return messageid;
    }

    public void setMessage_id(Integer message_id) {
        this.messageid = message_id;
    }

    public Integer getSender_id() {
        return sender_id;
    }

    public void setSender_id(Integer sender_id) {
        this.sender_id = sender_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public Timestamp getSent_date() {
        return sent_date;
    }

    public void setSent_date(Timestamp sent_date) {
        this.sent_date = sent_date;
    }

    public Integer getParent_message_id() {
        return parent_message_id;
    }

    public void setParent_message_id(Integer parent_message_id) {
        this.parent_message_id = parent_message_id;
    }

    public String getTrigger() {
        return trigger_name;
    }

    public void setTrigger(String trigger) {
        this.trigger_name = trigger;
    }

    public UUID getMessage_uid() {
        return message_uid;
    }

    public void setMessage_uid(UUID message_uid) {
        this.message_uid = message_uid;
    }

    public String getIsDelivered() {
        return isDelivered;
    }

    public void setIsDelivered(String isDelivered) {
        this.isDelivered = isDelivered;
    }
}
