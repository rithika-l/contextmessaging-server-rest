package hello;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "AIM_INVITATIONS")
public class AimInvitation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int invite_id;

    @Column(name = "user_id")
    int userId;
    int referred_by;
    Timestamp date_joined;

    public int getInvite_id() {
        return invite_id;
    }

    public void setInvite_id(int invitation_id) {
        this.invite_id = invitation_id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getReferred_by() {
        return referred_by;
    }

    public void setReferred_by(int referred_by) {
        this.referred_by = referred_by;
    }

    public Timestamp getDate_joined() {
        return date_joined;
    }

    public void setDate_joined(Timestamp date_joined) {
        this.date_joined = date_joined;
    }

    public AimInvitation() {
        this.userId = -1;
    }
}
