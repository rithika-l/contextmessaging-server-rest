package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "STEPS")
public class StepsGoal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer steps_id;

    private Integer goal_id;
    private Integer steps_set;
    private Integer steps_taken;

    public Integer getSteps_id() {
        return steps_id;
    }

    public void setSteps_id(Integer steps_id) {
        this.steps_id = steps_id;
    }

    public Integer getGoal_id() {
        return goal_id;
    }

    public void setGoal_id(Integer goal_id) {
        this.goal_id = goal_id;
    }

    public Integer getSteps_set() {
        return steps_set;
    }

    public void setSteps_set(Integer steps_set) {
        this.steps_set = steps_set;
    }

    public Integer getSteps_taken() {
        return steps_taken;
    }

    public void setSteps_taken(Integer steps_taken) {
        this.steps_taken = steps_taken;
    }
}
