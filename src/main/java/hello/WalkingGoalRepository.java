package hello;

import org.springframework.data.repository.CrudRepository;

public interface WalkingGoalRepository extends CrudRepository<WalkingGoal, Integer> {

}
