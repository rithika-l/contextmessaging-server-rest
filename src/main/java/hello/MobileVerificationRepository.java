package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MobileVerificationRepository extends CrudRepository<MobileVerification, Integer > {
    Optional<MobileVerification> findByMobile(String mobile);
}
