package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface ActivityTriggerRepository extends CrudRepository<ActivityTrigger, Integer> {
    Iterable<ActivityTrigger> findAllByIsDeliveredEquals(String isDeliveredValue);

    Optional<ActivityTrigger> findByMessageId(int messageId);
}
