package hello;

import org.springframework.data.repository.CrudRepository;

public interface UserRegistrationTokenRepository extends CrudRepository<UserRegistrationToken, Integer> {

}
