package hello;


import javax.persistence.*;

@Entity(name="USER_REGISTRATION_TOKEN")
public class UserRegistrationToken {
    @Id
    private Integer user_id;

    private String registration_token;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getRegistration_token() {
        return registration_token;
    }

    public void setRegistration_token(String registration_token) {
        this.registration_token = registration_token;
    }

}
