package hello.controllers;


import hello.MessagingToken;
import hello.MessagingTokenRepository;
import hello.Utils.RandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/tokens")
public class MessagingTokenController {

    @Autowired
    MessagingTokenRepository messagingTokenRepository;

    @PostMapping(path = "/add")
    public @ResponseBody MessagingToken generateToken(@RequestBody int userId){
        MessagingToken m = new MessagingToken();
        m.setUserId(userId);
        m.setToken(RandomGenerator.generateNewToken());
        messagingTokenRepository.save(m);
        return m;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<MessagingToken> getAll(){
        return messagingTokenRepository.findAll();
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public @ResponseBody MessagingToken getByUerId(@RequestParam("id") int id){
        Optional<MessagingToken> m = messagingTokenRepository.findByUserId(id);
        return m.orElse(null);
    }
}
