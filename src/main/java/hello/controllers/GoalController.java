package hello.controllers;

import hello.Goal;
import hello.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/apis/goals")
public class GoalController {

    @Autowired
    GoalRepository goalRepository;

    @PostMapping(path = "/add-goal")
    public @ResponseBody String addNewGoal(@RequestBody Goal goal){
        Goal g = new Goal();
        g.setGoal_id(goal.getGoal_id());
        g.setGoal_end_time(goal.getGoal_end_time());
        g.setGoal_start_date(goal.getGoal_start_date());
        g.setGoal_type(goal.getGoal_type());
        g.setUser_id(goal.getUser_id());
        goalRepository.save(g);
        return "Saved";
    }

    @GetMapping(path = "/all-goals")
    public @ResponseBody Iterable<Goal> getAllGoals(){
        return goalRepository.findAll();
    }
}
