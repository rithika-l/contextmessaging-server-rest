package hello.controllers;

import hello.AimInvitation;
import hello.AimInvitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@Controller
@RequestMapping(path = "apis/aim-installs")
public class AimInvitationController {

    @Autowired
    AimInvitationRepository aimInvitationRepository;

    @PostMapping(path="/add")
    public @ResponseBody AimInvitation addInstallDetails(@RequestBody AimInvitation aimInvitation){
        AimInvitation a = aimInvitationRepository.findByUserId(aimInvitation.getUserId()).orElse(new AimInvitation());
        if(a.getUserId()==-1) {
            a.setUserId(aimInvitation.getUserId());
            a.setReferred_by(aimInvitation.getReferred_by());
            a.setDate_joined(new Timestamp(System.currentTimeMillis()));
            aimInvitationRepository.save(a);
        }
        return a;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<AimInvitation> getAllAInstallsByReferrals(){
        return aimInvitationRepository.findAll();
    }
}
