package hello.controllers;

import hello.LocationTrigger;
import hello.LocationTriggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/location-triggers")
public class LocationTriggerController {

    @Autowired
    LocationTriggerRepository locationTriggerRepository;

    @PostMapping(path = "/add")
    public @ResponseBody LocationTrigger addLocationTrigger(@RequestBody LocationTrigger locationTrigger){
        LocationTrigger l = new LocationTrigger();
        l.setLatitude(locationTrigger.getLatitude());
        l.setLongitude(locationTrigger.getLongitude());
        l.setMessageId(locationTrigger.getMessageId());
        l.setPlace(locationTrigger.getPlace());
        l.setRadius(locationTrigger.getRadius());
        l.setRule(locationTrigger.getRule());
        l.setIsDelivered("FALSE");
        locationTriggerRepository.save(l);
        return l;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<LocationTrigger> getAllLocationTriggers(){
        return locationTriggerRepository.findAllByIsDeliveredEquals("FALSE");
    }

    @PostMapping(path = "/update")
    public @ResponseBody LocationTrigger updateLocationTrigger(@RequestBody LocationTrigger locationTrigger){
        LocationTrigger l = locationTriggerRepository.findById(locationTrigger.getTrigger_location_id()).get();
        l.setIsDelivered(locationTrigger.getIsDelivered());
        locationTriggerRepository.save(l);
        return l;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody LocationTrigger getTriggerByMessageId(@RequestParam("message-id") Integer message_id){
        Optional<LocationTrigger> l= locationTriggerRepository.findByMessageId(message_id);
        return l.orElse(null);
    }
}
