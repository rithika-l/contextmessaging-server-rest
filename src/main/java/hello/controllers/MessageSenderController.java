package hello.controllers;

import hello.MessageSender;
import hello.MessageSenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/message-senders")
public class MessageSenderController {

    @Autowired
    MessageSenderRepository messageSenderRepository;

    @PostMapping(path = "/add")
    public @ResponseBody MessageSender addMessageSender(@RequestBody MessageSender messageSender){
        Date date= new Date();
        //getTime() returns current time in milliseconds
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        MessageSender m = new MessageSender();
        m.setAttachment(messageSender.getAttachment());
        m.setBody(messageSender.getBody());
        m.setParent_message_id(messageSender.getParent_message_id());
        m.setSender_id(messageSender.getSender_id());
        m.setSent_date(ts);
        m.setIsDelivered("FALSE");
        m.setSubject(messageSender.getSubject());
        m.setTrigger(messageSender.getTrigger());
        messageSenderRepository.save(m);
        return m;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<MessageSender> getAllMessageSenders(){
        return messageSenderRepository.findAll();
    }

    @GetMapping(path = "/unsent")
    public @ResponseBody Iterable<MessageSender> getAllUnsentMessages() {
        return messageSenderRepository.getAllByIsDeliveredEquals("FALSE");
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody MessageSender getMessageSenderById(@RequestParam("id") Integer message_id){
        Optional<MessageSender> m= messageSenderRepository.findByMessageid(message_id);
        return m.orElse(null);
    }

    @PostMapping(path = "/update")
    public @ResponseBody MessageSender updateMessageSender(@RequestBody MessageSender messageSender){
        MessageSender m = messageSenderRepository.findById(messageSender.getMessage_id()).get();
        m.setIsDelivered(messageSender.getIsDelivered());
        messageSenderRepository.save(m);
        return m;
    }
}
