package hello.controllers;


import hello.UserGroup;
import hello.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/apis/user-group")
public class UserGroupController {

    @Autowired
    UserGroupRepository userGroupRepository;

    @PostMapping(path = "/add-user-to-group")
    public @ResponseBody String addUserToGroup(@RequestBody UserGroup userGroup){
        UserGroup u = new UserGroup();
        u.setGroup_id(userGroup.getGroup_id());
        u.setIs_active(userGroup.getIs_active());
        u.setJoin_date(userGroup.getJoin_date());
        u.setNotification_sent(userGroup.getNotification_sent());
        u.setUser_group_id(userGroup.getUser_group_id());
        u.setUser_id(userGroup.getUser_id());
        userGroupRepository.save(u);
        return "Saved";
    }

    @GetMapping(path = "/all-user-groups")
    public @ResponseBody Iterable<UserGroup> getAllgroups(){
        return userGroupRepository.findAll();
    }

}
