package hello.controllers;

import hello.StepsGoal;
import hello.StepsGoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/apis/steps-goal")
public class StepsGoalController {

    @Autowired
    StepsGoalRepository stepsGoalRepository;

    @PostMapping(path = "/add-steps-goal")
    public @ResponseBody String addStepsGoal(@RequestBody StepsGoal stepsGoal){
        StepsGoal s = new StepsGoal();
        s.setGoal_id(stepsGoal.getGoal_id());
        s.setSteps_id(stepsGoal.getSteps_id());
        s.setSteps_set(stepsGoal.getSteps_set());
        s.setSteps_taken(stepsGoal.getSteps_taken());
        stepsGoalRepository.save(s);
        return "Saved";
    }

    @GetMapping(path = "/all-steps-goals")
    public @ResponseBody Iterable<StepsGoal> getAllStepsGoals(){
        return stepsGoalRepository.findAll();
    }
}
