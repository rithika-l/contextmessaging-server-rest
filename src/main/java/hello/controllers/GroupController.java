package hello.controllers;

import hello.Group;
import hello.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/apis/groups")
public class GroupController {

    @Autowired
    GroupRepository groupRepository;

    @PostMapping(path = "/add-group")
    public @ResponseBody String addGroup(@RequestBody Group group){
        Group g = new Group();
        g.setCreate_date(group.getCreate_date());
        g.setDescription(group.getDescription());
        g.setGroup_id(group.getGroup_id());
        g.setIs_active(group.getIs_active());
        g.setName(group.getName());
        groupRepository.save(g);
        return "Saved";
    }

    @GetMapping(path = "/all-groups")
    public @ResponseBody Iterable<Group> getAllGroups(){
        return groupRepository.findAll();
    }
}
