package hello.controllers;

import hello.WalkingGoal;
import hello.WalkingGoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/apis/walking-goal")
public class WalkingGoalController {

    @Autowired
    WalkingGoalRepository walkingGoalRepository;

    @PostMapping(path = "/add-walking-goal")
    public @ResponseBody String addWalkingGoal(@RequestBody WalkingGoal walkingGoal){
        WalkingGoal w = new WalkingGoal();
        w.setActual_distance(walkingGoal.getActual_distance());
        w.setDistance(walkingGoal.getDistance());
        w.setDuration(walkingGoal.getDuration());
        w.setGoal_id(walkingGoal.getGoal_id());
        w.setWalking_id(walkingGoal.getWalking_id());
        walkingGoalRepository.save(w);
        return "Saved";
    }

    @GetMapping(path = "/all-walking-goals")
    public @ResponseBody Iterable<WalkingGoal> getAllWalkingGoals(){
        return walkingGoalRepository.findAll();
    }
}
