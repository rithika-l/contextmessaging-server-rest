package hello.controllers;

import hello.UserRegistrationToken;
import hello.UserRegistrationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/user-registration-token")
public class UserRegistrationController {

    @Autowired
    UserRegistrationTokenRepository userRegistrationTokenRepository;

    @PostMapping(path = "/add")
    public @ResponseBody String addUserRegistrationToken(@RequestBody UserRegistrationToken userRegistrationToken){
        UserRegistrationToken u = new UserRegistrationToken();
        u.setRegistration_token(userRegistrationToken.getRegistration_token());
        u.setUser_id(userRegistrationToken.getUser_id());
        userRegistrationTokenRepository.save(u);
        return "Saved";
    }

    @PostMapping(path = "/update")
    public @ResponseBody String updateRegistrationToken(@RequestBody UserRegistrationToken userRegistrationToken ){
       UserRegistrationToken u = userRegistrationTokenRepository.findById(userRegistrationToken.getUser_id()).get();
       u.setRegistration_token(userRegistrationToken.getRegistration_token());
       userRegistrationTokenRepository.save(u);
       return "Updated";
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<UserRegistrationToken> getAllTokens(){
        return userRegistrationTokenRepository.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody UserRegistrationToken getRegistrationToken(@RequestParam("user-id") Integer user_id){
        Optional<UserRegistrationToken> u = userRegistrationTokenRepository.findById(user_id);
        return u.orElse(new UserRegistrationToken());
    }
}
