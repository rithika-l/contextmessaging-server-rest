package hello.controllers;

import hello.ActivityTrigger;
import hello.ActivityTriggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/activity-triggers")
public class ActivityTriggerController {

    @Autowired
    ActivityTriggerRepository activityTriggerRepository;

    @PostMapping(path = "/add")
    public @ResponseBody String addActivityTrigger(@RequestBody ActivityTrigger a){
        ActivityTrigger activityTrigger = new ActivityTrigger();
        activityTrigger.setExpire_time(a.getExpire_time());
        activityTrigger.setMessageId(a.getMessageId());
        activityTrigger.setRule(a.getRule());
        activityTrigger.setTrigger_activity_id(a.getTrigger_activity_id());
        activityTrigger.setIsDelivered("FALSE");
        activityTriggerRepository.save(activityTrigger);
        return "Saved";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody ActivityTrigger getTriggerByMessageId(@RequestParam("message-id") Integer message_id){
        Optional<ActivityTrigger> a= activityTriggerRepository.findByMessageId(message_id);
        return a.orElse(null);
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<ActivityTrigger> getAllActivityTriggers(){
        return activityTriggerRepository.findAllByIsDeliveredEquals("FALSE");
    }

    @PostMapping(path = "/update")
    public @ResponseBody ActivityTrigger updateActivityTrigger(@RequestBody ActivityTrigger activityTrigger){
        ActivityTrigger a = activityTriggerRepository.findById(activityTrigger.getTrigger_activity_id()).get();
        a.setIsDelivered(activityTrigger.getIsDelivered());
        activityTriggerRepository.save(a);
        return a;
    }
}
