package hello.controllers;

import hello.User;
import hello.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping(path="/add")
    public @ResponseBody User addNewUser (@RequestBody User user){
        User n = new User();
        n.setName(user.getName());
        n.setUser_id(user.getUser_id());
        n.setLoginid(user.getLoginid());
        n.setLogin_pin("12345");
        n.setCharacter(user.getCharacter());
        n.setIs_active(user.isIs_active());
        n.setStart_date(user.getStart_date());
        n.setMobile(user.getMobile());
        n.setLast_timezone(user.getLast_timezone());
        n.setIs_location_present("FALSE");
        userRepository.save(n);
        return n;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<User> getAllUsers(){
        return userRepository.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody User getUserByUsername(@RequestParam("username") String username){
        Optional<User> u = userRepository.findByLoginid(username);
        return u.orElse(null);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public @ResponseBody User getUserById(@RequestParam("id") int id){
        Optional<User> u = userRepository.findByUserid(id);
        return u.orElse(null);
    }

    @PostMapping(path = "/verify")
    public @ResponseBody User verifyMobile (@RequestBody String mobile){
        Optional<User> u = userRepository.findByMobile(mobile);
        return u.orElse(null);
    }

    @PostMapping(path = "/update")
    public @ResponseBody User updateUser(@RequestBody User user){
        User u = userRepository.findByUserid(user.getUser_id()).get();
        u.setLast_timezone(user.getLast_timezone());
        u.setName(user.getName());
        userRepository.save(u);
        return u;
    }

}
