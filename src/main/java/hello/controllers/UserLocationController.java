package hello.controllers;

import hello.UserLocation;
import hello.UserLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/apis/user-locations")
public class UserLocationController {

    @Autowired
    UserLocationRepository userLocationRepository;

    @PostMapping(path = "/add")
    public @ResponseBody UserLocation addUserLocation(@RequestBody UserLocation userLocation){
        UserLocation u = new UserLocation();
        u.setUserId(userLocation.getUserId());
        u.setLabel(userLocation.getLabel());
        userLocationRepository.save(u);
        return u;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<UserLocation> getAllLocations(){
        return userLocationRepository.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody List<UserLocation> findAllByUserId(@RequestParam("user-id") Integer userId){
        return userLocationRepository.findAllByUserId(userId);
    }
}
