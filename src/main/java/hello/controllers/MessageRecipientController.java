package hello.controllers;

import hello.MessageRecipient;
import hello.MessageRecipientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping(path = "/apis/message-recipients")
public class MessageRecipientController {

    @Autowired
    MessageRecipientRepository messageRecipientRepository;

    @PostMapping(path = "/add")
    public @ResponseBody MessageRecipient addMessageRecipient(@RequestBody MessageRecipient messageRecipient){
        MessageRecipient m = new MessageRecipient();
        m.setIs_read(messageRecipient.getIs_read());
        m.setMessageid(messageRecipient.getMessageid());
        m.setNotification_sent(messageRecipient.getNotification_sent());
        m.setRecipient_group_id(messageRecipient.getRecipient_group_id());
        m.setRecipient_id(messageRecipient.getRecipient_id());
        messageRecipientRepository.save(m);
        return m;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<MessageRecipient> getAllMessageRecipients(){
        return messageRecipientRepository.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody MessageRecipient getMessageSenderById(@RequestParam("id") UUID message_id){
        Optional<MessageRecipient> m= messageRecipientRepository.findByMessageid(message_id);
        return m.orElse(null);
    }
}
