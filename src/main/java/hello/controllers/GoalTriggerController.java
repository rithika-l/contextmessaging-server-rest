package hello.controllers;

import hello.GoalTrigger;
import hello.GoalTriggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/apis/goal-triggers")
public class GoalTriggerController {

    @Autowired
    GoalTriggerRepository goalTriggerRepository;

    @PostMapping(path = "/add-goal-trigger")
    public @ResponseBody String addGoalTrigger(@RequestBody GoalTrigger goalTrigger){
        GoalTrigger g = new GoalTrigger();
        g.setGoal_id(goalTrigger.getGoal_id());
        g.setMessage_id(goalTrigger.getMessage_id());
        g.setRule(goalTrigger.getRule());
        g.setTrigger_goal_id(goalTrigger.getTrigger_goal_id());
        goalTriggerRepository.save(g);
        return "Saved";
    }

    @GetMapping(path = "/all-goal-triggers")
    public @ResponseBody Iterable<GoalTrigger> getAllGoalTriggers(){
        return goalTriggerRepository.findAll();
    }

}
