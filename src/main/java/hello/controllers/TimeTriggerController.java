package hello.controllers;

import hello.TimeTrigger;
import hello.TimeTriggerRepository;
import hello.Utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping(path = "/apis/time-triggers")
public class TimeTriggerController {

    @Autowired
    TimeTriggerRepository timeTriggerRepository;

    @PostMapping(path = "/add")
    public @ResponseBody TimeTrigger addTimeTrigger(@RequestBody TimeTrigger timeTrigger){
        TimeTrigger t = new TimeTrigger();
        t.setExpire_time(timeTrigger.getExpire_time());
        t.setFrequency_friday(timeTrigger.getFrequency_friday());
        t.setFrequency_interval(timeTrigger.getFrequency_interval());
        t.setFrequency_monday(timeTrigger.getFrequency_monday());
        t.setFrequency_saturday(timeTrigger.getFrequency_saturday());
        t.setFrequency_sunday(timeTrigger.getFrequency_sunday());
        t.setFrequency_thursday(timeTrigger.getFrequency_thursday());
        t.setFrequency_tuesday(timeTrigger.getFrequency_tuesday());
        t.setFrequency_type(timeTrigger.getFrequency_type());
        t.setFrequency_wednesday(timeTrigger.getFrequency_wednesday());
        t.setMessageId(timeTrigger.getMessageId());
        t.setRule(timeTrigger.getRule());
        t.setTime(timeTrigger.getTime());
        t.setDay_of_week(timeTrigger.getDay_of_week());
        t.setTimezone(timeTrigger.getTimezone());
        t.setTrigger_time_id(timeTrigger.getTrigger_time_id());
        t.setMulTriggers(timeTrigger.getMulTriggers());

        if(t.getMulTriggers().equalsIgnoreCase("FALSE")){
            t.setScheduledTimestamp(Timestamp.valueOf(Util.convertToTimestamp(timeTrigger)));
        }
        timeTriggerRepository.save(t);
        return t;
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<TimeTrigger> getAllTimeTriggers(){
        Date date= new Date();
        //getTime() returns current time in milliseconds
        long time = date.getTime();
        long endTime = time + 5000;
        //Passed the milliseconds to constructor of Timestamp class
        Timestamp end = new Timestamp(endTime);
        return timeTriggerRepository.findAllByScheduledTimestampBeforeAndMulTriggersEquals(end, "FALSE");
    }

    @PostMapping(path = "/update")
    public @ResponseBody TimeTrigger updateLocationTrigger(@RequestBody TimeTrigger timeTrigger){
        TimeTrigger t = timeTriggerRepository.findById(timeTrigger.getTrigger_time_id()).get();
        timeTriggerRepository.save(t);
        return t;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody TimeTrigger getTriggerByMessageId(@RequestParam("message-id") Integer message_id){
        Optional<TimeTrigger> t= timeTriggerRepository.findByMessageId(message_id);
        return t.orElse(null);
    }
}
