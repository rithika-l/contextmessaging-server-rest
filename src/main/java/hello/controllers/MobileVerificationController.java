package hello.controllers;

import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import hello.MobileVerification;
import hello.MobileVerificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping(path = "/apis/mobile-verification")
public class MobileVerificationController {

    @Autowired
    MobileVerificationRepository mobileVerificationRepository;

    public static final String ACCOUNT_SID = "AC9c746946bc071ce80506a499586a2db1";
    public static final String AUTH_TOKEN = "0451d9af068a29adbe48b5531222aeff";
    public static final String SERVICE_SID = "VA4395ed8bfb6d6e3c13798090309cda5a";

    @PostMapping(path = "/add")
    public @ResponseBody MobileVerification addMobileForVerification(@RequestBody MobileVerification mobileVerification){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Verification verification = Verification.creator(
                SERVICE_SID,
                mobileVerification.getMobile(),
                "sms")
                .create();

        MobileVerification m = mobileVerificationRepository.findByMobile(mobileVerification.getMobile()).orElse(new MobileVerification());
        if(m.getMobile()!=null){
            m.setVerification_status(verification.getStatus());
            Date expirationDate = new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10));
            m.setExpiration_time(new Timestamp(expirationDate.getTime()));
        }
        else {
            m.setMobile(mobileVerification.getMobile());
            m.setVerification_status("PENDING");
            Date expirationDate = new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10));
            m.setExpiration_time(new Timestamp(expirationDate.getTime()));
        }
        mobileVerificationRepository.save(m);
        return m;
    }

    @PostMapping(path = "/verify")
    public @ResponseBody MobileVerification verifyCode(@RequestBody MobileVerification mobileVerification){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        VerificationCheck verificationCheck = VerificationCheck.creator(
                SERVICE_SID,
                mobileVerification.getOtp())
                .setTo(mobileVerification.getMobile()).create();

        MobileVerification serverData = mobileVerificationRepository.findByMobile(mobileVerification.getMobile()).orElse(new MobileVerification());
        if(serverData.getMobile()!=null){
            if(verificationCheck.getStatus().equalsIgnoreCase("approved")){
                serverData.setVerification_status("VERIFIED");
                mobileVerificationRepository.save(serverData);
            }
        }
        return serverData;
    }
}
