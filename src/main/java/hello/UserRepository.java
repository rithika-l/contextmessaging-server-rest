package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    Optional<User> findByLoginid(String id);

    Optional<User> findByUserid(int id);

    Optional<User> findByMobile(String mobile);
}
