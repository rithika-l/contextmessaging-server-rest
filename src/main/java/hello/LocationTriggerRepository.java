package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LocationTriggerRepository extends CrudRepository<LocationTrigger, Integer> {
    Iterable<LocationTrigger> findAllByIsDeliveredEquals(String isDelivered);

    Optional<LocationTrigger> findByMessageId(int messageId);
}
