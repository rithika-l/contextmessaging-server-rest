package hello;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name = "USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", columnDefinition = "INT(11)")
    private Integer userid;

    @Column(columnDefinition = "TINYTEXT")
    private String name;
    @Column(name = "character_name", columnDefinition = "TINYTEXT")
    private String character;
    @Column(columnDefinition = "TINYTEXT")
    private String login_pin;
    @Column(name = "login_id", columnDefinition = "TINYTEXT")
    private String loginid;
    @Column(columnDefinition = "DATE")
    private Date start_date;
    @Column(columnDefinition = "TINYTEXT")
    private String is_active;
    @Column(columnDefinition = "TINYTEXT")
    private String mobile;
    @Column(columnDefinition = "TINYTEXT")
    private String last_timezone;
    @Column(columnDefinition = "TIMESTAMP")
    private Timestamp last_updated;
    @Column(columnDefinition = "TINYTEXT")
    private String is_location_present;

    public Integer getUser_id() {
        return userid;
    }

    public void setUser_id(Integer user_id) {
        this.userid = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getLogin_pin() {
        return login_pin;
    }

    public void setLogin_pin(String login_pin) {
        this.login_pin = login_pin;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public String isIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Timestamp getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(Timestamp sync_last_updated) {
        this.last_updated = sync_last_updated;
    }

    public String getLast_timezone() {
        return last_timezone;
    }

    public void setLast_timezone(String last_timezone) {
        this.last_timezone = last_timezone;
    }

    public String getIs_location_present() {
        return is_location_present;
    }

    public void setIs_location_present(String is_location_present) {
        this.is_location_present = is_location_present;
    }
}
